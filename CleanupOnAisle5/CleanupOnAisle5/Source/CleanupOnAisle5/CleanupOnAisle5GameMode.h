// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CleanupOnAisle5GameMode.generated.h"

UCLASS(minimalapi)
class ACleanupOnAisle5GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ACleanupOnAisle5GameMode();
};



