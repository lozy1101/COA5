// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class CleanupOnAisle5 : ModuleRules
{
	public CleanupOnAisle5(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
