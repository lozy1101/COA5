// Copyright Epic Games, Inc. All Rights Reserved.

#include "CleanupOnAisle5GameMode.h"
#include "CleanupOnAisle5Character.h"
#include "UObject/ConstructorHelpers.h"

ACleanupOnAisle5GameMode::ACleanupOnAisle5GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
