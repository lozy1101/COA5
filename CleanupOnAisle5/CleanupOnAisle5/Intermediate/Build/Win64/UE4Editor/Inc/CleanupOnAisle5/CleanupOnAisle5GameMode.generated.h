// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CLEANUPONAISLE5_CleanupOnAisle5GameMode_generated_h
#error "CleanupOnAisle5GameMode.generated.h already included, missing '#pragma once' in CleanupOnAisle5GameMode.h"
#endif
#define CLEANUPONAISLE5_CleanupOnAisle5GameMode_generated_h

#define CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5GameMode_h_12_SPARSE_DATA
#define CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5GameMode_h_12_RPC_WRAPPERS
#define CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5GameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACleanupOnAisle5GameMode(); \
	friend struct Z_Construct_UClass_ACleanupOnAisle5GameMode_Statics; \
public: \
	DECLARE_CLASS(ACleanupOnAisle5GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/CleanupOnAisle5"), CLEANUPONAISLE5_API) \
	DECLARE_SERIALIZER(ACleanupOnAisle5GameMode)


#define CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5GameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesACleanupOnAisle5GameMode(); \
	friend struct Z_Construct_UClass_ACleanupOnAisle5GameMode_Statics; \
public: \
	DECLARE_CLASS(ACleanupOnAisle5GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/CleanupOnAisle5"), CLEANUPONAISLE5_API) \
	DECLARE_SERIALIZER(ACleanupOnAisle5GameMode)


#define CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5GameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	CLEANUPONAISLE5_API ACleanupOnAisle5GameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACleanupOnAisle5GameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CLEANUPONAISLE5_API, ACleanupOnAisle5GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACleanupOnAisle5GameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	CLEANUPONAISLE5_API ACleanupOnAisle5GameMode(ACleanupOnAisle5GameMode&&); \
	CLEANUPONAISLE5_API ACleanupOnAisle5GameMode(const ACleanupOnAisle5GameMode&); \
public:


#define CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	CLEANUPONAISLE5_API ACleanupOnAisle5GameMode(ACleanupOnAisle5GameMode&&); \
	CLEANUPONAISLE5_API ACleanupOnAisle5GameMode(const ACleanupOnAisle5GameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CLEANUPONAISLE5_API, ACleanupOnAisle5GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACleanupOnAisle5GameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACleanupOnAisle5GameMode)


#define CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5GameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5GameMode_h_9_PROLOG
#define CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5GameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5GameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5GameMode_h_12_SPARSE_DATA \
	CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5GameMode_h_12_RPC_WRAPPERS \
	CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5GameMode_h_12_INCLASS \
	CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5GameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5GameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5GameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5GameMode_h_12_SPARSE_DATA \
	CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5GameMode_h_12_INCLASS_NO_PURE_DECLS \
	CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CLEANUPONAISLE5_API UClass* StaticClass<class ACleanupOnAisle5GameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5GameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
