// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CleanupOnAisle5/CleanupOnAisle5GameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCleanupOnAisle5GameMode() {}
// Cross Module References
	CLEANUPONAISLE5_API UClass* Z_Construct_UClass_ACleanupOnAisle5GameMode_NoRegister();
	CLEANUPONAISLE5_API UClass* Z_Construct_UClass_ACleanupOnAisle5GameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_CleanupOnAisle5();
// End Cross Module References
	void ACleanupOnAisle5GameMode::StaticRegisterNativesACleanupOnAisle5GameMode()
	{
	}
	UClass* Z_Construct_UClass_ACleanupOnAisle5GameMode_NoRegister()
	{
		return ACleanupOnAisle5GameMode::StaticClass();
	}
	struct Z_Construct_UClass_ACleanupOnAisle5GameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ACleanupOnAisle5GameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_CleanupOnAisle5,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACleanupOnAisle5GameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "CleanupOnAisle5GameMode.h" },
		{ "ModuleRelativePath", "CleanupOnAisle5GameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ACleanupOnAisle5GameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ACleanupOnAisle5GameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ACleanupOnAisle5GameMode_Statics::ClassParams = {
		&ACleanupOnAisle5GameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_ACleanupOnAisle5GameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ACleanupOnAisle5GameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ACleanupOnAisle5GameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ACleanupOnAisle5GameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACleanupOnAisle5GameMode, 463581453);
	template<> CLEANUPONAISLE5_API UClass* StaticClass<ACleanupOnAisle5GameMode>()
	{
		return ACleanupOnAisle5GameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACleanupOnAisle5GameMode(Z_Construct_UClass_ACleanupOnAisle5GameMode, &ACleanupOnAisle5GameMode::StaticClass, TEXT("/Script/CleanupOnAisle5"), TEXT("ACleanupOnAisle5GameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACleanupOnAisle5GameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
