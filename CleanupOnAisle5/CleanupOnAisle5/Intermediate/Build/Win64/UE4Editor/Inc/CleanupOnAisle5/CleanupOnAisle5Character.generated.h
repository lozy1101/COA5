// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CLEANUPONAISLE5_CleanupOnAisle5Character_generated_h
#error "CleanupOnAisle5Character.generated.h already included, missing '#pragma once' in CleanupOnAisle5Character.h"
#endif
#define CLEANUPONAISLE5_CleanupOnAisle5Character_generated_h

#define CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5Character_h_12_SPARSE_DATA
#define CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5Character_h_12_RPC_WRAPPERS
#define CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5Character_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5Character_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACleanupOnAisle5Character(); \
	friend struct Z_Construct_UClass_ACleanupOnAisle5Character_Statics; \
public: \
	DECLARE_CLASS(ACleanupOnAisle5Character, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CleanupOnAisle5"), NO_API) \
	DECLARE_SERIALIZER(ACleanupOnAisle5Character)


#define CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5Character_h_12_INCLASS \
private: \
	static void StaticRegisterNativesACleanupOnAisle5Character(); \
	friend struct Z_Construct_UClass_ACleanupOnAisle5Character_Statics; \
public: \
	DECLARE_CLASS(ACleanupOnAisle5Character, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CleanupOnAisle5"), NO_API) \
	DECLARE_SERIALIZER(ACleanupOnAisle5Character)


#define CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5Character_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACleanupOnAisle5Character(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACleanupOnAisle5Character) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACleanupOnAisle5Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACleanupOnAisle5Character); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACleanupOnAisle5Character(ACleanupOnAisle5Character&&); \
	NO_API ACleanupOnAisle5Character(const ACleanupOnAisle5Character&); \
public:


#define CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5Character_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACleanupOnAisle5Character(ACleanupOnAisle5Character&&); \
	NO_API ACleanupOnAisle5Character(const ACleanupOnAisle5Character&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACleanupOnAisle5Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACleanupOnAisle5Character); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACleanupOnAisle5Character)


#define CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5Character_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(ACleanupOnAisle5Character, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(ACleanupOnAisle5Character, FollowCamera); }


#define CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5Character_h_9_PROLOG
#define CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5Character_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5Character_h_12_PRIVATE_PROPERTY_OFFSET \
	CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5Character_h_12_SPARSE_DATA \
	CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5Character_h_12_RPC_WRAPPERS \
	CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5Character_h_12_INCLASS \
	CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5Character_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5Character_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5Character_h_12_PRIVATE_PROPERTY_OFFSET \
	CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5Character_h_12_SPARSE_DATA \
	CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5Character_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5Character_h_12_INCLASS_NO_PURE_DECLS \
	CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5Character_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CLEANUPONAISLE5_API UClass* StaticClass<class ACleanupOnAisle5Character>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CleanupOnAisle5_Source_CleanupOnAisle5_CleanupOnAisle5Character_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
